Source: kicad
Section: electronics
Priority: optional
Maintainer: Jean-Samuel Reynaud <js.reynaud@gmail.com>
Build-Depends: debhelper (>= 11), cmake (>= 2.6.0), doxygen, libbz2-dev, libcairo2-dev, libglu1-mesa-dev,
 lld,
 unixodbc-dev,
 libgl1-mesa-dev, libglew-dev, libx11-dev, libwxgtk3.2-dev,
 mesa-common-dev, pkg-config, libssl-dev, build-essential, cmake-curses-gui, debhelper, grep,
 python3-dev (>= 3.6), swig, dblatex, po4a, asciidoc,
 python3-wxgtk4.0,
 source-highlight, libboost-all-dev, libglm-dev (>= 0.9.5.1) | libglm-kicad-dev, libcurl4-openssl-dev,
 libgtk-3-dev,  libngspice0-dev,  ngspice-dev,
 libocct-foundation-dev, libocct-modeling-algorithms-dev, libocct-modeling-data-dev , libocct-ocaf-dev, libocct-visualization-dev,
 libocct-data-exchange-dev, zlib1g-dev
Standards-Version: 3.9.3
Homepage: http://www.kicad.org

Package: kicad-nightly
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends},  libngspice0, xsltproc, kicad-nightly-demos
Recommends: kicad-nightly-libraries, kicad-nightly-doc-en
Suggests: extra-xdg-menus,
	kicad-nightly-doc-en | kicad-nightly-doc-fr | kicad-nightly-doc-ja | kicad-nightly-doc-it | kicad-nightly-doc-nl | kicad-nightly-doc-pl,
    kicad-nightly-locale-ko | kicad-nightly-locale-ru | kicad-nightly-locale-pl | kicad-nightly-locale-pt | kicad-nightly-locale-ja | kicad-nightly-locale-id |
    kicad-nightly-locale-ca | kicad-nightly-locale-it | kicad-nightly-locale-el | kicad-nightly-locale-zh | kicad-nightly-locale-de | kicad-nightly-locale-sl |
    kicad-nightly-locale-cs | kicad-nightly-locale-bg | kicad-nightly-locale-sv | kicad-nightly-locale-lt | kicad-nightly-locale-fi | kicad-nightly-locale-fr |
    kicad-nightly-locale-hu | kicad-nightly-locale-nl | kicad-nightly-locale-es | kicad-nightly-locale-sk
Description: Electronic schematic and PCB design software
 KiCad is a suite of programs for the creation of printed circuit boards.
 It includes a schematic editor, a PCB layout tool, support tools and a
 3D viewer to display a finished & fully populated PCB.
 .
 KiCad is made up of 4 main components:
 .
  * KiCad project manager
  * Schematic editor
  * PCB editor
  * GERBER viewer
 .
 Libraries:
  * Both the schematic and PCB editors have library managers and editors for their
    components and footprints
  * You can easily create, edit, delete and exchange library items
  * Documentation files can be associated with components, footprints and key
    words, allowing a fast search by function
  * Very large libraries are available for schematic components and footprints
  * Most components have corresponding 3D models


Package: kicad-nightly-dbg
Architecture: any
Depends: kicad-nightly (= ${binary:Version}), ${misc:Depends}
Description: Debug symbols for kicad
 KiCad is a suite of programs for the creation of printed circuit boards.
 It includes a schematic editor, a PCB layout tool, support tools and a
 3D viewer to display a finished & fully populated PCB.
 .
 KiCad is made up of 5 main components:
 .
  * kicad - project manager
  * eeschema - schematic editor
  * pcbnew - PCB editor
  * gerbview - GERBER viewer
  * cvpcb - footprint selector for components
 .
 Libraries:
  * Both eeschema and pcbnew have library managers and editors for their
    components and footprints
  * You can easily create, edit, delete and exchange library items
  * Documentation files can be associated with components, footprints and key
    words, allowing a fast search by function
  * Very large libraries are available for schematic components and footprints
  * Most components have corresponding 3D models


Package: kicad-nightly-demos
Architecture: all
Depends: ${misc:Depends}
Recommends: kicad-nightly
Replaces: kicad-nightly-demo
Conflicts: kicad-nightly-demo
Breaks: kicad-nightly-demo
Description: Common files used by kicad
 This package contains the component libraries and language files for KiCad.
